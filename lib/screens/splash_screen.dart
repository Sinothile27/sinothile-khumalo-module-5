import 'dart:async';

import 'package:flutter/material.dart';
import 'package:smg_app/reusable_widgets/reusable_widget.dart';
import 'package:smg_app/screens/signin_screen.dart';

// Create a project on Firebase
// Connect your web app to Firebase
// Add a form to your app with at least 3 input fields
// Write code to connect your app to the database to create, read, update and delete

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _isVisible = false;

  _SplashScreenState() {
    new Timer(const Duration(milliseconds: 2000), () {
      setState(() {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => SignInScreen()),
            (route) => false);
      });
    });

    new Timer(Duration(milliseconds: 10), () {
      setState(() {
        _isVisible =
            true; // Now it is showing fade effect and navigating to Login page
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
          colors: [
            Theme.of(context).accentColor,
            Theme.of(context).primaryColor
          ],
          begin: const FractionalOffset(0, 0),
          end: const FractionalOffset(1.0, 0.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp,
        ),
      ),
      child: AnimatedOpacity(
        opacity: _isVisible ? 1.0 : 0,
        duration: Duration(milliseconds: 1200),
        child: Center(
            child: Column(
          children: <Widget>[
            const SizedBox(
              height: 265,
            ),
            logoWidget("assets/images/logo.png"),
            const SizedBox(
              height: 30,
            ),
          ],
        )),
      ),
    );
  }
}
